<?php

namespace Yoda\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Yoda\EventBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setEmail('admin@admin.pl');
        $userAdmin->setPassword('test');
        $userAdmin->setUsername('admin');
        $userAdmin->setAlias('0');
        $userAdmin->setCountry('Poland');

        $manager->persist($userAdmin);
        $manager->flush();
    }
}