<?php

namespace Yoda\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
//    public function indexAction($name)
//    {
//        return $this->render('YodaEventBundle:Default:index.html.twig', array('name' => $name));
//    }
    
    public function indexAction($name,$count=null)
    {
//        if ($count==null){
//            return $this->render('YodaEventBundle:Default:index.html.twig', array('name' => $name));
//        }else {
//            var_dump($name, $count);die;
//        }
//        if($count!=null){    
//            $arr = array(
//            'firstName' => $name,
//            'count'     => $count,
//            'status'    => 'It\'s a traaaaaaaap!',
//            );
//
//            $response = new Response(json_encode($arr));
//            $response->headers->set('Content-Type', 'application/json');
//
//            return $response;
//        }else {
//            $templating = $this->container->get('templating');
//
//            $content = $templating->render(
//                'YodaEventBundle:Default:index.html.twig',
//                array('name' => $name,'count' =>$count)
//            );
//            return new Response($content);
//        }
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('YodaEventBundle:User');
            $event = $repo->findOneBy(array(
            'username' => 'gieroj',
            ));
            return $this->render(
        'YodaEventBundle:Default:index.html.twig',
        array(
            'name' => $name,
            'count' => $count,
            'event'=> $event,
        )
    );
    }
}
